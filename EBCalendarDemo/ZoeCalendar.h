//
//  ZoeCalendar.h
//  EBCalendarDemo
//
//  Created by Zoe on 15/11/10.
//  Copyright © 2015年 Zoe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EBDateButton : UIButton

@property (nonatomic, assign) NSInteger year;
@property (nonatomic, assign) NSInteger month;
@property (nonatomic, assign) NSInteger day;

@end

@class ZoeCalendar;

@protocol ZoeCalendarDelegate <NSObject>

@optional

- (void)selectedYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day;

@end

// Zoe日历系统，支持frame和autolayout两种编程模式
@interface ZoeCalendar : UIView

@property (nonatomic, weak) id<ZoeCalendarDelegate> delegate;

- (void)loadMonthViewForMonth:(NSInteger)month Year:(NSInteger)year;

- (void)loadWeekViewForDay:(NSInteger)day month:(NSInteger)month Year:(NSInteger)year;

@end
