//
//  AppDelegate.h
//  EBCalendarDemo
//
//  Created by Zoe on 15/11/10.
//  Copyright © 2015年 Zoe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

