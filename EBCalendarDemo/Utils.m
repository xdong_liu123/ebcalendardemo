//
//  Utils.m
//  EBCalendarDemo
//
//  Created by Zoe on 15/11/10.
//  Copyright © 2015年 Zoe. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (NSInteger)daysNumberForMonth:(NSInteger)month year:(NSInteger)year {
    
    NSInteger days = 0;
    
    switch(month) {
        case 1:case 3:case 5:case 7:case 8:case 10:case 12:
            days = 31;
            break;
        case 4:case 6:case 9:case 11:
            days = 30;
            break;
        case 2:
            if ((year%4 == 0 && year%100 != 0) || year%400 == 0)
                days = 29;
            else
                days = 28;
            break;
        default:
            [NSException raise:NSRangeException format:@"month is invalid"];
    }
    return days;
}


+ (NSInteger)weekDayOfDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year {
    NSString *dateStr = [NSString stringWithFormat:@"%.4i-%.2i-%.2i", year, month, day];
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setLocale:[NSLocale currentLocale]];
    [inputFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate* date = [inputFormatter dateFromString:dateStr];
    return [self weekDayOfDay:date];
}


+ (NSInteger)weekDayOfDay:(NSDate *)date {
    
    NSCalendar *greCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    //  通过已定义的日历对象，获取某个时间点的NSDateComponents表示，并设置需要表示哪些信息（NSYearCalendarUnit, NSMonthCalendarUnit, NSDayCalendarUnit等）
    NSDateComponents *dateComponents = [greCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekday | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekOfYear fromDate:date];
    
    return dateComponents.weekday;
}


+ (NSInteger)dateOfDay:(NSDate *)date {
    
    NSCalendar *greCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    //  通过已定义的日历对象，获取某个时间点的NSDateComponents表示，并设置需要表示哪些信息（NSYearCalendarUnit, NSMonthCalendarUnit, NSDayCalendarUnit等）
    NSDateComponents *dateComponents = [greCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekday | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekOfYear fromDate:date];
    
    return dateComponents.day;
}


+ (NSInteger)monthOfDay:(NSDate *)date {
    
    NSCalendar *greCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    //  通过已定义的日历对象，获取某个时间点的NSDateComponents表示，并设置需要表示哪些信息（NSYearCalendarUnit, NSMonthCalendarUnit, NSDayCalendarUnit等）
    NSDateComponents *dateComponents = [greCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekday | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekOfYear fromDate:date];
    
    return dateComponents.month;
}


+ (NSInteger)yearOfDay:(NSDate *)date {
    
    NSCalendar *greCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    //  通过已定义的日历对象，获取某个时间点的NSDateComponents表示，并设置需要表示哪些信息（NSYearCalendarUnit, NSMonthCalendarUnit, NSDayCalendarUnit等）
    NSDateComponents *dateComponents = [greCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekday | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekOfYear fromDate:date];
    
    return dateComponents.year;
}


@end
