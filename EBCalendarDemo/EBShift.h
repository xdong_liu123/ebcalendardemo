//
//  EBShift.h
//  EBCalendarDemo
//
//  Created by Zoe on 15/11/19.
//  Copyright © 2015年 Zoe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EBShift : NSObject

@property (nonatomic, copy) NSString *shiftName;
@property (nonatomic, copy) NSString *calendar;

@property (nonatomic, copy) NSString *startTime;
@property (nonatomic, copy) NSString *endTime;

@end
