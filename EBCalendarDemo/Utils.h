//
//  Utils.h
//  EBCalendarDemo
//
//  Created by Zoe on 15/11/10.
//  Copyright © 2015年 Zoe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

// 某年某月有多少天
+ (NSInteger)daysNumberForMonth:(NSInteger)month year:(NSInteger)year;

// 某年某月某日是星期几，Sunday:1, Monday:2, Tuesday:3, Wednesday:4, Thursday, Friday:6, Saturday:7
+ (NSInteger)weekDayOfDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year;

// 某个日期是星期几，Sunday:1, Monday:2, Tuesday:3, Wednesday:4, Thursday, Friday:6, Saturday:7
+ (NSInteger)weekDayOfDay:(NSDate *)date;

+ (NSInteger)yearOfDay:(NSDate *)date;

+ (NSInteger)monthOfDay:(NSDate *)date;

+ (NSInteger)dateOfDay:(NSDate *)date;

@end
