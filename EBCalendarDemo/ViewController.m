//
//  ViewController.m
//  EBCalendarDemo
//
//  Created by Zoe on 15/11/10.
//  Copyright © 2015年 Zoe. All rights reserved.
//

#import "ViewController.h"
#import "ZoeCalendar.h"
#import "EBShift.h"
#import "EBShiftTableViewCell.h"
#import "Utils.h"

#define KSCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define KSCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

@interface ViewController () <ZoeCalendarDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet ZoeCalendar *calendar;

@property (nonatomic, strong) NSArray *shifts;

@property (nonatomic, copy) NSString *currentDay;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.calendar.delegate = self;
    
    self.tableView.bounces = NO;
    self.tableView.showsVerticalScrollIndicator = NO;
    
    NSDate *date = [NSDate date];
    
    self.currentDay = [NSString stringWithFormat:@"%.4i %.2i %.2i", [Utils yearOfDay:date],
                       [Utils monthOfDay:date], [Utils dateOfDay:date]];
    
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView Datasource &delegate

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.currentDay;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;// self.shifts.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *indentifier = @"EBShiftTableViewCell";
    EBShiftTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"EBShiftTableViewCell"
                                              owner:self options:nil] lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}



#pragma mark - ZoeCalendarDelegate Methods

- (void)selectedYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day {
    self.currentDay = [NSString stringWithFormat:@"%.4i %.2i %.2i", year, month, day];
    [self.tableView reloadData];
}

@end
