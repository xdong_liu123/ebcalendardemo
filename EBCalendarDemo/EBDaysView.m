//
//  EBDaysView.m
//  EBCalendarDemo
//
//  Created by Zoe on 15/11/12.
//  Copyright © 2015年 Zoe. All rights reserved.
//

#import "EBDaysView.h"

@implementation EBDaysView

- (NSMutableArray *)dayAry {
    if (!_dayAry) {
        _dayAry = [NSMutableArray array];
    }
    return _dayAry;
}

@end
