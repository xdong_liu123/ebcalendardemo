//
//  ZoeCalendar.m
//  EBCalendarDemo
//
//  Created by Zoe on 15/11/10.
//  Copyright © 2015年 Zoe. All rights reserved.
//

#import "ZoeCalendar.h"
#import "Utils.h"
#import "EBDaysView.h"

#define CalendarHeaderHeight 50
#define TempMonthViewTag 0x999999
#define CalendarForeGroundColor [UIColor colorWithRed:0x00/255.0 green:0x00/255.0 blue:0x00/255.0 alpha:1.0]

typedef enum : NSUInteger {
    kCalendarMonthMode,
    kCalendarWeekMode
} CalendarDisplayMode;

@implementation EBDateButton


@end


@interface ZoeCalendar () <UIScrollViewDelegate>

@property (nonatomic, strong) UIView *calendarHeader;
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, assign) CalendarDisplayMode displayMode;
@property (nonatomic, assign) CGFloat calendarWidth;

@property (nonatomic, strong) UIScrollView *calendarContainer;

@property (nonatomic, assign) NSInteger currentYear;  // 日历显示的当前年
@property (nonatomic, assign) NSInteger currentMonth; // 日历显示的当前月，仅适用kCalendarMonthMode
@property (nonatomic, assign) NSInteger someDayOfCurrentWeek;   // 日历显示的当前周中某一天

@property (nonatomic, assign) NSInteger selectYear;  // 当前选择的年
@property (nonatomic, assign) NSInteger selectMonth; // 当前选择的月
@property (nonatomic, assign) NSInteger selectDay;   // 当前选择的日

@property (nonatomic, strong) NSMutableArray *monthViews;
@property (nonatomic, strong) NSMutableArray *weekViews;

@end


@implementation ZoeCalendar

#pragma mark - Public Methods

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.calendarWidth  = frame.size.width;
        [self initComponents];
    }
    return self;
}


- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {

    }
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.calendarWidth = self.frame.size.width;
    [self initComponents];
}


- (void)initComponents {
    self.backgroundColor = [UIColor colorWithRed:0xF5/255.0
                                           green:0xF5/255.0
                                            blue:0xF5/255.0 alpha:1.0];
    self.clipsToBounds = YES;
    
    _calendarContainer = [[UIScrollView alloc] init];
    _calendarContainer.showsHorizontalScrollIndicator = NO;
    _calendarContainer.showsVerticalScrollIndicator   = NO;
    _calendarContainer.pagingEnabled = YES;
    _calendarContainer.bounces       = NO;
    _calendarContainer.delegate      = self;
    [self addSubview:_calendarContainer];
    // 加手势检测
    [self addPanGestureToContainer:_calendarContainer];
    
    // Construct Calendar Header View
    [self createCalendarHeader];
    
    // init select date
    NSDate *date = [NSDate date];
    
    [self setSelectYear:[Utils yearOfDay:date]
            selectMonth:[Utils monthOfDay:date]
              selectDay:[Utils dateOfDay:date]];
    
    [self loadMonthViewForMonth:[Utils monthOfDay:date]
                           Year:[Utils yearOfDay:date]];
}



- (void)loadMonthViewForMonth:(NSInteger)month Year:(NSInteger)year {
    
    self.displayMode = kCalendarMonthMode;

    [self clearOldViews];
    
    self.currentMonth = month;
    self.currentYear  = year;
    
    EBDaysView *currentMonthView = [self createMonthViewForYear:year month:month];
    
    // 更新最外层view frame
    CGRect oldFrame = self.frame;
    
    self.frame = CGRectMake(oldFrame.origin.x, oldFrame.origin.y,
                            self.calendarWidth, currentMonthView.bounds.size.height + CalendarHeaderHeight);
    self.calendarContainer.frame = CGRectMake(0, CalendarHeaderHeight, self.calendarWidth, currentMonthView.bounds.size.height);
    
    // 更新header title
    CGPoint olderCenter = self.titleLabel.center;
    self.titleLabel.text = [NSString stringWithFormat:@"%@ %i", [self monthName:month], year];
    [self.titleLabel sizeToFit];
    self.titleLabel.center = olderCenter;

    currentMonthView.frame = CGRectMake(self.calendarWidth, 0,
                                        self.calendarWidth, currentMonthView.bounds.size.height);
    
    [self.calendarContainer addSubview:currentMonthView];
    
    EBDaysView *nextMonthView = nil;
    if (month == 12) {
        nextMonthView = [self createMonthViewForYear:year + 1 month:1];
    } else {
        nextMonthView = [self createMonthViewForYear:year month:month + 1];
    }
    
    nextMonthView.frame = CGRectMake(2 * self.calendarWidth, 0, self.calendarWidth, currentMonthView.bounds.size.height);
    
    [self.calendarContainer addSubview:nextMonthView];
    
    EBDaysView *preMonthView = nil;
    if (month == 1) {
        preMonthView = [self createMonthViewForYear:year - 1 month:12];
    } else {
        preMonthView = [self createMonthViewForYear:year month:month - 1];
    }
    
    preMonthView.frame = CGRectMake(0, 0, self.calendarWidth, currentMonthView.bounds.size.height);
    
    [self.calendarContainer addSubview:preMonthView];
    
    // 预存在数组中
    [self.monthViews addObject:preMonthView];
    [self.monthViews addObject:currentMonthView];
    [self.monthViews addObject:nextMonthView];
    
    self.calendarContainer.contentSize = CGSizeMake(self.calendarWidth * 3, _calendarContainer.bounds.size.height);
    
    [self.calendarContainer setContentOffset:CGPointMake(self.calendarWidth, 0)];
    
    // 通知autolayout机制重新计算内省尺寸
    [self invalidateIntrinsicContentSize];
}


- (void)loadWeekViewForDay:(NSInteger)day month:(NSInteger)month Year:(NSInteger)year {
    
    self.displayMode = kCalendarWeekMode;
    
    [self clearOldViews];
    
    self.currentMonth = month;
    self.currentYear  = year;
    self.someDayOfCurrentWeek = day;
    
    EBDaysView *currentWeekView = [self createWeekViewForDay:day month:month year:year];
    
    // 更新最外层view frame
    CGRect oldFrame = self.frame;
    
    self.frame = CGRectMake(oldFrame.origin.x, oldFrame.origin.y,
                            self.calendarWidth, currentWeekView.bounds.size.height + CalendarHeaderHeight);
    self.calendarContainer.frame = CGRectMake(0, CalendarHeaderHeight, self.calendarWidth, currentWeekView.bounds.size.height);
    
    // 更新header title
    EBDateButton *firstDayBtn = currentWeekView.dayAry[0];
    EBDateButton *endDayBtn = currentWeekView.dayAry[6];
    if (firstDayBtn.month == endDayBtn.month) {
        self.titleLabel.text = [NSString stringWithFormat:@"%@ %i", [self monthName:month], year];
    } else {
        self.titleLabel.text = [NSString stringWithFormat:@"%@ %i - %@ %i",
                                [self simplifyMonthName:firstDayBtn.month], firstDayBtn.year,
                                [self simplifyMonthName:endDayBtn.month], endDayBtn.year];
    }
    CGPoint olderCenter = self.titleLabel.center;
    [self.titleLabel sizeToFit];
    self.titleLabel.center = olderCenter;
    
    currentWeekView.frame = CGRectMake(self.calendarWidth, 0, self.calendarWidth, currentWeekView.bounds.size.height);
    
    [self.calendarContainer addSubview:currentWeekView];
    
    EBDaysView *priorWeekView;
    if (day - 7 > 0) { // 仍在本月
        priorWeekView = [self createWeekViewForDay:day - 7 month:month year:year];
    } else { // 上月
        if (month == 1) {
            NSInteger newDay = [Utils daysNumberForMonth:12 year:year - 1] + day - 7;
            priorWeekView = [self createWeekViewForDay:newDay month:12 year:year - 1];
        } else {
            NSInteger newDay = [Utils daysNumberForMonth:month - 1 year:year] + day - 7;
            priorWeekView = [self createWeekViewForDay:newDay month:month - 1 year:year];
        }
    }
    
    priorWeekView.frame = CGRectMake(0, 0, self.calendarWidth, currentWeekView.bounds.size.height);
    
    [self.calendarContainer addSubview:priorWeekView];
    
    EBDaysView *nextWeekView;
    NSInteger monthDayNum = [Utils daysNumberForMonth:month year:year];
    if (day + 7 > monthDayNum) { // 下月某天
        if (month == 12) {
            nextWeekView = [self createWeekViewForDay:day + 7 - monthDayNum month:1 year:year + 1];
        } else {
            nextWeekView = [self createWeekViewForDay:day + 7 - monthDayNum month:month + 1 year:year];
        }
    } else {
        nextWeekView = [self createWeekViewForDay:day + 7 month:month year:year];
    }
    
    nextWeekView.frame = CGRectMake(2 * self.calendarWidth, 0, self.calendarWidth, currentWeekView.bounds.size.height);
    
    [self.calendarContainer addSubview:nextWeekView];
    
    // 预存在数组中
    [self.weekViews addObject:priorWeekView];
    [self.weekViews addObject:currentWeekView];
    [self.weekViews addObject:nextWeekView];
    
    self.calendarContainer.contentSize = CGSizeMake(self.calendarWidth * 3, _calendarContainer.bounds.size.height);
    
    [self.calendarContainer setContentOffset:CGPointMake(self.calendarWidth, 0)];
    
    // 通知autolayout机制重新计算内省尺寸
    [self invalidateIntrinsicContentSize];
}


#pragma mark - Getters &Setters

- (NSMutableArray *)monthViews {
    if (!_monthViews) {
        _monthViews = [NSMutableArray array];
    }
    return _monthViews;
}


- (NSMutableArray *)weekViews {
    if (!_weekViews) {
        _weekViews = [NSMutableArray array];
    }
    return _weekViews;
}


#pragma mark - Action Methods

- (void)todayClick {
    NSDate *date = [NSDate date];
    
    [self setSelectYear:[Utils yearOfDay:date]
            selectMonth:[Utils monthOfDay:date]
              selectDay:[Utils dateOfDay:date]];
    
    [self loadMonthViewForMonth:[Utils monthOfDay:date]
                           Year:[Utils yearOfDay:date]];
}


- (IBAction)dateBtnClicked:(id)sender {
    EBDateButton *dayBtn = sender;
    
    [self setSelectYear:dayBtn.year
            selectMonth:dayBtn.month
              selectDay:dayBtn.day];
    
    [self loadMonthViewForMonth:dayBtn.month Year:dayBtn.year];
    
//    if (self.displayMode == kCalendarMonthMode) {
//        [self loadWeekViewForDay:dayBtn.day month:dayBtn.month Year:dayBtn.year];
//    } else {
//        [self loadWeekViewForDay:dayBtn.day month:dayBtn.month Year:dayBtn.year];
//    }
}


- (void)panRecognized:(UIPanGestureRecognizer *)recognizer {
    
    CGPoint translation = [recognizer translationInView:recognizer.view];
    // CGPoint velocity    = [recognizer velocityInView:recognizer.view];
    
    static CGRect orgCalendarFrm;
    static CGRect orgContainerFrm;
    
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan: {
            
            orgCalendarFrm  = self.frame;
            orgContainerFrm = self.calendarContainer.frame;
            
            [self handlePanGestureWithPanWidth:translation.y
                                     orgCalFrm:orgCalendarFrm
                               orgContainerFrm:orgContainerFrm
                                         isEnd:NO];
            break;
        }
        case UIGestureRecognizerStateChanged: {
            [self handlePanGestureWithPanWidth:translation.y
                                     orgCalFrm:orgCalendarFrm
                               orgContainerFrm:orgContainerFrm
                                         isEnd:NO];
            break;
        }
        case UIGestureRecognizerStateEnded:
            [self handlePanGestureWithPanWidth:translation.y
                                     orgCalFrm:orgCalendarFrm
                               orgContainerFrm:orgContainerFrm
                                         isEnd:YES];
            break;
        case UIGestureRecognizerStateFailed:
            [self handlePanGestureWithPanWidth:translation.y
                                     orgCalFrm:orgCalendarFrm
                               orgContainerFrm:orgContainerFrm
                                         isEnd:YES];
            break;
        case UIGestureRecognizerStateCancelled: {

            [self handlePanGestureWithPanWidth:translation.y
                                     orgCalFrm:orgCalendarFrm
                               orgContainerFrm:orgContainerFrm
                                         isEnd:YES];
        }
        default:
            break;
    }
}


- (void)handlePanGestureWithPanWidth:(CGFloat)width
                           orgCalFrm:(CGRect)orgCalFrm
                     orgContainerFrm:(CGRect)orgContainerFrm
                               isEnd:(BOOL)end {
    CGFloat btnHeight  = self.calendarWidth / 7.0;
    
    if (self.displayMode == kCalendarMonthMode) { // 当前为monthView
        // 边界条件，手势结束时无论滑动方向如何都必须处理，否则会出bug
        if (end) {
            //width = width > 0 ? 0 : width;
        }
        if (width <= 0) {
            NSInteger destLine = [self selectedDateLineinDaysView:self.monthViews[1]];
            CGFloat yOffset = destLine < 0 ? 0 : (destLine - 1) * btnHeight;
            CGRect destFrame = CGRectMake(0, CalendarHeaderHeight - yOffset,
                                          orgContainerFrm.size.width, btnHeight);
            
            CGFloat panDistanceFactor = width / (orgContainerFrm.size.height - destFrame.size.height);
            CGFloat panDistance = (destFrame.origin.y - orgContainerFrm.origin.y) * panDistanceFactor;
            
            if (end) {
                if (-panDistanceFactor >= 0.6) {
                    // 转到当前选择日期所在的WeekView
                    [self loadWeekViewForDay:self.selectDay month:self.selectMonth Year:self.selectYear];
                    
                } else {
                    self.calendarContainer.frame = orgContainerFrm;
                    self.frame = orgCalFrm;
                    // 通知autolayout机制重新计算内省尺寸
                    [self invalidateIntrinsicContentSize];
                }
                
            } else {
                if (CalendarHeaderHeight - panDistance >= destFrame.origin.y) {
                    self.calendarContainer.frame = CGRectMake(0, CalendarHeaderHeight - panDistance,
                                                              orgContainerFrm.size.width,
                                                              orgContainerFrm.size.height);
                    self.frame = (CGRect){orgCalFrm.origin, CGSizeMake(orgCalFrm.size.width, orgCalFrm.size.height + width)};
                    // 通知autolayout机制重新计算内省尺寸
                    [self invalidateIntrinsicContentSize];
                }
            }
        }
    } else { // 当前为WeekView
        // 边界条件，手势结束时无论滑动方向如何都必须处理，否则会出bug
        if (end) {
           width = width < 0 ? 0 : width;
        }
        if (width >= 0) {
            EBDaysView *currentWeekView = self.weekViews[1];
            currentWeekView.hidden = YES; // 隐藏weekView
            EBDaysView *tmpMonthView = (EBDaysView *)[self.calendarContainer viewWithTag:TempMonthViewTag];
            
            if (!tmpMonthView) {
                tmpMonthView = [self createMonthViewForYear:self.selectYear month:self.selectMonth];
                tmpMonthView.tag   = TempMonthViewTag;
                tmpMonthView.frame = CGRectMake(self.calendarWidth, 0,
                                                self.calendarWidth, tmpMonthView.frame.size.height);
                [self.calendarContainer addSubview:tmpMonthView];
            }
            
            CGFloat maxDrawDistance = tmpMonthView.frame.size.height - btnHeight; // 最大拖行距离
            
            CGFloat panDistanceFactor = width / maxDrawDistance;
            
            NSInteger layLine = [self selectedDateLineinDaysView:tmpMonthView];
            
            CGFloat startY = CalendarHeaderHeight - (layLine - 1) * btnHeight; // 起始y坐标
            
            CGFloat yMoveOffset = (layLine - 1) * btnHeight * panDistanceFactor; // y方向移动距离
            
            if (end) {
                [tmpMonthView removeFromSuperview];
                if (panDistanceFactor > 0.6) {
                    [self loadMonthViewForMonth:self.selectMonth Year:self.selectYear];
                } else {
                    currentWeekView.hidden = NO;
                    self.calendarContainer.frame = orgContainerFrm;
                    self.frame = orgCalFrm;
                    // 通知autolayout机制重新计算内省尺寸
                    [self invalidateIntrinsicContentSize];
                }
                
            } else {
                if (width <= maxDrawDistance) {
                    self.frame = (CGRect){orgCalFrm.origin, CGSizeMake(orgCalFrm.size.width,
                                                                       orgCalFrm.size.height + width)};
                    self.calendarContainer.frame = CGRectMake(0, startY + yMoveOffset,
                                                              self.calendarWidth,
                                                              tmpMonthView.bounds.size.height);
                    // 通知autolayout机制重新计算内省尺寸
                    [self invalidateIntrinsicContentSize];
                }
            }
        }
    }
}


#pragma mark - Utils Methods

- (void)createCalendarHeader {
    
    UIView *calendarHeader =
    [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.calendarWidth, CalendarHeaderHeight)];
    [self addSubview:calendarHeader];
    calendarHeader.backgroundColor = [UIColor colorWithRed:0xF5/255.0
                                                     green:0xF5/255.0
                                                      blue:0xF5/255.0 alpha:1.0];
    self.calendarHeader = calendarHeader;
    
    UIButton *todayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    todayBtn.frame = CGRectMake(10, 5, 40, 25);
    [todayBtn setTitle:@"Today" forState:UIControlStateNormal];
    [todayBtn setTitleColor:CalendarForeGroundColor forState:UIControlStateNormal];
    [todayBtn addTarget:self action:@selector(todayClick) forControlEvents:UIControlEventTouchUpInside];
    todayBtn.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14];
    [calendarHeader addSubview:todayBtn];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text      = @"July 2015";
    titleLabel.textColor = [UIColor colorWithRed:0xF5/255.0 green:0x57/255.0 blue:0x18/255.0 alpha:1.0];
    titleLabel.font      = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14];
    [titleLabel sizeToFit];
    self.titleLabel = titleLabel;
    titleLabel.center = CGPointMake(self.calendarWidth / 2.0, todayBtn.center.y);
    [self addSubview:titleLabel];
    
    UIView *weekTitleHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 40, self.calendarWidth, 14)];
    NSArray *titleAry = @[@"Su",
                          @"M",
                          @"T",
                          @"W",
                          @"Th",
                          @"F",
                          @"S"];
    for (int i = 0; i < 7; ++i) {
        UILabel *weekDayLabel = [[UILabel alloc] initWithFrame:CGRectMake(i * (self.calendarWidth / 7.0), 0, self.calendarWidth / 7.0, 14)];
        weekDayLabel.text = titleAry[i];
        weekDayLabel.textColor = [UIColor colorWithRed:0x99/255.0 green:0x99/255.0 blue:0x99/255.0 alpha:1.0];
        weekDayLabel.font          = [UIFont systemFontOfSize:13];
        weekDayLabel.textAlignment = NSTextAlignmentCenter;
        [weekTitleHeader addSubview:weekDayLabel];
    }
    
    [calendarHeader addSubview:weekTitleHeader];
}


- (void)addPanGestureToContainer:(UIScrollView *)contaner {
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(panRecognized:)];
    [contaner addGestureRecognizer:panGesture];
    [panGesture requireGestureRecognizerToFail:contaner.panGestureRecognizer];
}


- (void)clearOldViews {
    for (UIView *subView in self.calendarContainer.subviews) {
        [subView removeFromSuperview];
    }
    [self.monthViews removeAllObjects];
    [self.weekViews removeAllObjects];
}


- (EBDaysView *)createMonthViewForYear:(NSInteger)year month:(NSInteger)month {
    
    NSInteger monthDaysNum = [Utils daysNumberForMonth:month year:year];
    
    NSInteger weekDay = [Utils weekDayOfDay:1 month:month year:year];
    
    NSInteger lineNum = ceilf((weekDay - 1 + monthDaysNum) / 7.0);
    
    CGFloat btnWidth  = self.calendarWidth / 7.0;
    CGFloat btnHeight = btnWidth;
    
    CGFloat viewHeight = lineNum * btnHeight;
    
    EBDaysView *monthView = [[EBDaysView alloc] initWithFrame:CGRectMake(0, 0, self.calendarWidth, viewHeight)];
    
    for (int i = 0; i < lineNum; ++i) { // 行
        
        CGFloat yOffset = i * btnHeight;
        
        UIView *grayLine = [[UIView alloc] initWithFrame:CGRectMake(2, yOffset + 5, self.calendarWidth - 4, .9)];
        grayLine.backgroundColor = [UIColor colorWithRed:0xDA/255.0 green:0xDA/255.0 blue:0xDA/255.0 alpha:0.6];
        grayLine.alpha = .6;
        [monthView addSubview:grayLine];
        
        for (int j = 0; j < 7; ++j) {   // 列
            
            CGFloat xOffset = j * btnWidth;
            
            EBDateButton *dayBtn = [self createMonthDateButtonForOrder:i * 7 + j
                                                              forMonth:month year:year];
            dayBtn.frame = CGRectMake(xOffset, yOffset, btnWidth, btnHeight);
            
            [self filterSelectButton:dayBtn];
            
            [monthView addSubview:dayBtn];
            
            [monthView.dayAry addObject:dayBtn];
        }
    }
    
    return monthView;
}


- (EBDaysView *)createWeekViewForDay:(NSInteger)day
                           month:(NSInteger)month
                            year:(NSInteger)year {
    
    NSInteger weekDay = [Utils weekDayOfDay:day month:month year:year];
    
    CGFloat btnWidth  = self.calendarWidth / 7.0;
    CGFloat btnHeight = btnWidth;
    
    EBDaysView *weekView = [[EBDaysView alloc] initWithFrame:CGRectMake(0, 0, self.calendarWidth, btnHeight)];
    
    NSInteger startYear;
    NSInteger startMonth;
    NSInteger startDay;
    if (day - (weekDay - 1) <= 0) { // 本周起始为上月某天
        if (month == 1) {
            startMonth = 12;
            startYear  = year - 1;
        } else {
            startMonth = month - 1;
            startYear  = year;
        }
        
        NSInteger lastMonthDaysNum = [Utils daysNumberForMonth:startMonth year:startYear];
        startDay = lastMonthDaysNum - (day - (weekDay - 1));
        
    } else {
        startYear  = year;
        startMonth = month;
        startDay   = day - (weekDay - 1);
    }
    
    for (int j = 0; j < 7; ++j) { // 列
    
        CGFloat xOffset = j * btnWidth;
    
        EBDateButton *dayBtn = [self createWeekDateButtonForWeekday:(j + 1)
                                                           startDay:startDay
                                                         startMonth:startMonth
                                                          startYear:startYear];
        dayBtn.frame = CGRectMake(xOffset, 0, btnWidth, btnHeight);
        
        [self filterSelectButton:dayBtn];
        
        [weekView addSubview:dayBtn];
        
        [weekView.dayAry addObject:dayBtn];
    }
    
    return weekView;
}



- (EBDateButton *)createMonthDateButtonForOrder:(NSInteger)order forMonth:(NSInteger)month year:(NSInteger)year {
    
    EBDateButton *dayBtn = [EBDateButton buttonWithType:UIButtonTypeCustom];
    
    dayBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    
    NSInteger monthDaysNum = [Utils daysNumberForMonth:month year:year];
    
    NSInteger weekDay = [Utils weekDayOfDay:1 month:month year:year];
    
    UIColor *textColor = CalendarForeGroundColor;
    // 当月的日期，减去一号即可
    if ((order + 1) >= weekDay && (order + 1) <= weekDay - 1 + monthDaysNum) {
        dayBtn.year  = year;
        dayBtn.month = month;
        dayBtn.day   = order + 2 - weekDay;
    } else if ((order + 1) < weekDay) {  // 上一个月的日期
        NSInteger lastMonthNum = 0;
        if (month == 1) {
            lastMonthNum = [Utils daysNumberForMonth:12 year:year - 1];
            dayBtn.year  = year - 1;
            dayBtn.month = 12;
        } else {
            lastMonthNum = [Utils daysNumberForMonth:month - 1 year:year];
            dayBtn.year  = year;
            dayBtn.month = month - 1;
        }
        dayBtn.day   = lastMonthNum - (weekDay - 1) + (order + 1);
        
        textColor = [UIColor colorWithRed:0xA6/255.0
                                    green:0xB3/255.0
                                     blue:0xBB/255.0
                                    alpha:1.0];
    } else { // 下个月的日期
        dayBtn.day = order + 2 - weekDay - monthDaysNum;
        if (month == 12) {
            dayBtn.year  = year + 1;
            dayBtn.month = 1;
        } else {
            dayBtn.year  = year;
            dayBtn.month = month + 1;
        }
        textColor = [UIColor colorWithRed:0xA6/255.0
                                    green:0xB3/255.0
                                     blue:0xBB/255.0
                                    alpha:1.0];
    }
    
    [dayBtn setTitleColor:textColor forState:UIControlStateNormal];
    dayBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 2, 0);
    [dayBtn setTitle:[NSString stringWithFormat:@"%i", dayBtn.day] forState:UIControlStateNormal];
    [dayBtn addTarget:self action:@selector(dateBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return dayBtn;
}


- (EBDateButton *)createWeekDateButtonForWeekday:(NSInteger)weekDay
                                        startDay:(NSInteger)startDay
                                      startMonth:(NSInteger)startMonth
                                       startYear:(NSInteger)startYear   {
    
    EBDateButton *dayBtn = [EBDateButton buttonWithType:UIButtonTypeCustom];
    
    dayBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    
    NSInteger delta = weekDay - 1;
    
    NSInteger monthDaysNum = [Utils daysNumberForMonth:startMonth year:startYear];
    
    if (startDay + delta > monthDaysNum) { // 下个月的日期
        dayBtn.day = startDay + delta - monthDaysNum;
        if (startMonth == 12) {
            dayBtn.year  = startYear + 1;
            dayBtn.month = 1;
        } else {
            dayBtn.year  = startYear;
            dayBtn.month = startMonth + 1;
        }
    } else {
        dayBtn.year  = startYear;
        dayBtn.month = startMonth;
        dayBtn.day   = startDay + delta;
    }
    
    [dayBtn setTitleColor:CalendarForeGroundColor forState:UIControlStateNormal];
    dayBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 2, 0);
    [dayBtn setTitle:[NSString stringWithFormat:@"%i", dayBtn.day] forState:UIControlStateNormal];
    [dayBtn addTarget:self action:@selector(dateBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return dayBtn;
}


- (void)filterSelectButton:(EBDateButton *)dayBtn {
    if (dayBtn.year == _selectYear && dayBtn.month == _selectMonth && dayBtn.day == _selectDay) {
        
        UILabel *indicatorLabel = [[UILabel alloc] initWithFrame:CGRectInset(dayBtn.bounds, 9, 9)];
        indicatorLabel.text = [dayBtn titleForState:UIControlStateNormal];
        indicatorLabel.textAlignment = NSTextAlignmentCenter;
        indicatorLabel.font          = dayBtn.titleLabel.font;
        indicatorLabel.backgroundColor =
                             [UIColor colorWithRed:0xF5/255.0 green:0x57/255.0 blue:0x18/255.0 alpha:1.0];
        indicatorLabel.textColor           = [UIColor whiteColor];
        indicatorLabel.layer.cornerRadius  = indicatorLabel.frame.size.width / 2;
        indicatorLabel.layer.masksToBounds = YES;
        indicatorLabel.center = CGPointMake(dayBtn.bounds.size.width / 2, dayBtn.bounds.size.height / 2 - 2);
        
        [dayBtn addSubview:indicatorLabel];
    }
    {
        UILabel *dotLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 4, 4)];
        dotLabel.layer.cornerRadius  = 2;
        dotLabel.layer.masksToBounds = YES;
        dotLabel.backgroundColor = [UIColor colorWithRed:0xC1/255.0 green:0xC1/255.0 blue:0xC1/255.0 alpha:1.0];;
        dotLabel.center          = CGPointMake(dayBtn.bounds.size.width / 2, dayBtn.bounds.size.height / 2 + 17);
        [dayBtn addSubview:dotLabel];
    }
}


- (NSInteger)selectedDateLineinDaysView:(EBDaysView *)daysView {
    
    NSInteger daysNum = daysView.dayAry.count;
    
    for (int i = 0; i < daysNum; ++i) {
        EBDateButton *dayBtn = daysView.dayAry[i];
        if (dayBtn.day == _selectDay && dayBtn.month == _selectMonth && dayBtn.year == _selectYear) {
            return i / 7 + 1;
        }
    }
    return -1;
}


- (NSString *)monthName:(NSInteger)month {
    NSArray *monthNames = @[@"January", @"February", @"March", @"April", @"May", @"June",
                            @"July",    @"August",   @"September", @"October", @"November", @"December"];
    if (month >= 1 && month <= 12) {
        return monthNames[month - 1];
    } else {
        return @"";
    }
}


- (NSString *)simplifyMonthName:(NSInteger)month {
    NSArray *monthNames = @[@"Jan", @"Feb", @"Mar", @"Apr", @"May", @"Jun",
                            @"Jul",    @"Aug",   @"Sep", @"Oct", @"Nov", @"Dec"];
    if (month >= 1 && month <= 12) {
        return monthNames[month - 1];
    } else {
        return @"";
    }
}


- (void)setSelectYear:(NSInteger)newYear selectMonth:(NSInteger)newMonth selectDay:(NSInteger)newDay {
    if (newDay != _selectDay || newMonth != _selectMonth || newYear != _selectYear) {
        _selectYear  = newYear;
        _selectMonth = newMonth;
        _selectDay   = newDay;
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(selectedYear:month:day:)]) {
            [self.delegate selectedYear:_selectYear month:_selectMonth day:_selectDay];
        }
    }
}


#pragma mark - UIScrollView Delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if (self.displayMode == kCalendarMonthMode) {
        [self reloadMonthView:scrollView];
    } else {
        [self reloadWeekView:scrollView];
    }
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    return YES;
}


- (void)reloadMonthView:(UIScrollView *)scrollView {
    NSInteger newCurMonth = self.currentMonth;
    NSInteger newCurYear  = self.currentYear;
    if (scrollView.contentOffset.x < 10) { // 停在pre page
        if (self.currentMonth == 1) {
            newCurMonth = 12;
            newCurYear = self.currentYear - 1;
        } else {
            newCurMonth = self.currentMonth - 1;
            newCurYear = self.currentYear;
        }
    } else if (scrollView.contentOffset.x < 2 * scrollView.bounds.size.width + 10 &&
               scrollView.contentOffset.x > 2 * scrollView.bounds.size.width - 10)  { // 停在next page
        if (self.currentMonth == 12) {
            newCurMonth = 1;
            newCurYear  = self.currentYear + 1;
        } else {
            newCurMonth = self.currentMonth + 1;
            newCurYear  = self.currentYear;
        }
    }
    
    if (newCurYear != self.currentYear || newCurMonth != self.currentMonth) {
        [self loadMonthViewForMonth:newCurMonth Year:newCurYear];
    }
}


- (void)reloadWeekView:(UIScrollView *)scrollView {

    if (scrollView.contentOffset.x < 10) { // 停在pre page
        EBDaysView *preWeekView = self.weekViews[0];
        
        EBDateButton *firstDayBtn = preWeekView.dayAry[0];
        
        [self setSelectYear:firstDayBtn.year
                selectMonth:firstDayBtn.month
                  selectDay:firstDayBtn.day];
        
        [self loadWeekViewForDay:firstDayBtn.day month:firstDayBtn.month Year:firstDayBtn.year];
    } else if (scrollView.contentOffset.x < 2 * scrollView.bounds.size.width + 10 &&
               scrollView.contentOffset.x > 2 * scrollView.bounds.size.width - 10)  { // 停在next page
        
        EBDaysView *nextWeekView = self.weekViews[2];
        
        EBDateButton *firstDayBtn = nextWeekView.dayAry[0];
        
        [self setSelectYear:firstDayBtn.year
                selectMonth:firstDayBtn.month
                  selectDay:firstDayBtn.day];
        
        [self loadWeekViewForDay:firstDayBtn.day month:firstDayBtn.month Year:firstDayBtn.year];
    }
}


// For autolayout meschism

- (void)updateConstraints {
    // Update the constraints if needed
    
    [super updateConstraints];
}


- (CGSize)intrinsicContentSize {
    return self.frame.size;
}

@end
