//
//  EBDaysView.h
//  EBCalendarDemo
//
//  Created by Zoe on 15/11/12.
//  Copyright © 2015年 Zoe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EBDaysView : UIView

@property (nonatomic, strong) NSMutableArray *dayAry;

@end
