//
//  ZoeCalendarTests.m
//  EBCalendarDemo
//
//  Created by Zoe on 15/11/18.
//  Copyright © 2015年 Zoe. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ZoeCalendar.h"

@interface ZoeCalendarTests : XCTestCase <ZoeCalendarDelegate> {
    ZoeCalendar *_calendar;
}

@end

@implementation ZoeCalendarTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _calendar = [[ZoeCalendar alloc] initWithCalendarWidth:200 delegate:self];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)test_loadMonthView_invalidMonth_exceptionThrow {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    XCTAssertThrowsSpecificNamed([_calendar loadMonthViewForMonth:202 Year:1997]
                                 , NSException, NSRangeException, @"month is invalid!");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
